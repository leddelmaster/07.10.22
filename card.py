import random

class Card:
    def __init__(self, nominal, suit, ACE_1 = True):
        self.nominal = nominal
        self.suit  = suit
        self.ace = ACE_1
        self.points = self.get_points(nominal)
        self.long = self.get_long(nominal)
    
    def __repr__(self):
        s = f'repr {self.nominal}, {self.suit}'
        return s
    
    
    def __str__(self):
        s = f'string {self.nominal}, {self.suit}'
        return s    
    
    def get_long(self, nominal):
        ''' превращает номинал в название карты '''
        d = {
            "A" : "туз",
            "J" : "валет",
            "Q" : "дама",
            "K" : "король"}
        if nominal in d:
            return d[nominal]
        else:
            return str(nominal)
        
        
        
    def get_points(self, nominal):
        ''' берет номинал и возвращает очки по номиналу '''
        if nominal in [2, 3, 4, 5, 6, 7, 8, 9, 10]:
            return nominal
        elif nominal == 'A':
            if self.ace:
                return 1
            else:
                return 14
        else:
            k = ['J', 'Q', 'K']
            n = k.index(nominal) + 11
            return n
            

class Deck:
    def __init__(self):
        self.deck = []
        
    def __len__(self):
        N = len(self.deck)
        return N
        
    def new_deck(self):
        self.deck.clear()
        nominals = [2, 3, 4, 5, 6, 7, 8, 9, 10, 'J', 'Q', 'K', 'A']
        for i in nominals:
            for j in ['пики', 'черви', 'трефы', 'бубны']:
                c = Card(i,j, ACE_1 = False)
                self.add(c)
    def list_deck(self):
        for card in self.deck:
            print(card.long, card.suit, card.points)
    
    def shuffle_deck(self):
        ''' тасует колоду '''
        random.shuffle(self.deck)
        
    def add(self, card):
        self.deck.append(card)
    
    def get_card(self):
        return self.deck.pop(0)


class Gamer:
    def __init__(self, name):
        self.hand = []
        self.name = name
    
    def __getitem__(self, position):
        return self.hand[position + 1]
    
    def now_card(self):
        n = len(self.hand)
        return n
        

        
    def get_card(self, card):
        self.hand.append(card)
    
    def show_hand(self):
        print(f'Колода игрока {self.name}')
        for card in self.hand:
            print(card.long, card.suit, card.points)        
    
    def __len__(self):
        n = len(self.hand)
        return n

d1 = Deck()
d1.new_deck()
d1.shuffle_deck()

g1 = Gamer('Люк')
g2 = Gamer('Лея')

print(f'исходно {len(d1)} карт')

for i in range(5):
    g1.get_card(d1.get_card())
    
    print(f'У игрока 1 {len(g1)} карт')
    print(f'В колоде {len(d1)} карт')
    
    g2.get_card(d1.get_card())

print(f'сейчас тут {d1.__len__()} карт')

g1.show_hand()
g2.show_hand()

print(g1.hand)

print(Card(2, 'пики'))

print(g1[1], g2[1])

