from functools import total_ordering

@total_ordering
class Account:
    """A simple account class"""

    def __init__(self, owner, amount=0):
        """
        This is the constructor that lets us create
        objects from this class
        """
        self.owner = owner
        self.amount = amount
        self._transactions = []


    def __repr__(self):
        return f'Account({self.owner}, {self.amount})'

    def __str__(self):
        return f'Account of {self.owner} with starting amount: {self.amount}'
    
    def __len__(self):
        n = len(self._transactions)
        return n

    def __getitem__(self, position):
        if position == 0:
            return self.amount
        else:
            return self._transactions[position - 1]

    def __reversed__(self):
        return [i for i in reversed(self._transactions)]

    def add_transaction(self, amount):
    
        if not isinstance(amount, int):
            raise ValueError('please use int for amount')
            
        self._transactions.append(amount)
    

    def __lt__(self, other):
        return self.balance < other.balance
  
    def __eq__(self, other):
        return self.balance == other.balance  

    @property
    def balance(self):
        return self.amount + sum(self._transactions) 

    def __add__(self, other):
    
        owner = f'{self.owner}&{other.owner}'
        start_amount = self.balance + other.balance
        
        acc = Account(owner, start_amount)
            
        return acc
        
    def __call__(self):
        print(f'Start amount: {self.amount}')
        print('Transactions: ')
        for transaction in self:
            print(transaction)
        print(f'\nBalance: {self.balance}')
        
        
b = Account("Джек Бауэр", 100)
p = Account("Президент Палмер", 1000)

'''
print(b)
print([b])
print(repr(b))
print(str(b))
'''
b.add_transaction(1000)
b.add_transaction(888)
b.add_transaction(-90)
b.add_transaction(9000)
b.add_transaction(-700)
print(b.balance)

#print(len(b))
#print(b[0])

'''
for i in b:
    print(i)
'''

#print(list(reversed(b)))
#print(p < b)
#print(p == b)
print(p + b)
b()
